import {Employee} from "./Employee.js";

export class Programmer extends Employee {
    constructor(name, age, salary, languages) {
        super(name, age, salary);

        this.languages = languages;
    }

    get salary() {
        return `Salary *3 $${this._salary * 3}`;
    }
    set salary(newSalary) {
        return this._salary = newSalary;
    }
}