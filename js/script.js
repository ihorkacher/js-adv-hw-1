import {Employee} from "./Employee.js";
import {Programmer} from "./Programmer.js"


const employee = new Employee('John', 404, 404);
employee.name = 'John Deere';
employee.age = 25;
employee.salary = 2500;
console.log(employee.name, employee.age, employee.salary);


const firstDeveloper = new Programmer('Ihor', 999, 2500, ["js", 'c++', 'java', ['css', 'html']]);

firstDeveloper.name = 'Ihor Kacher';
firstDeveloper.age = 28;
console.log(firstDeveloper);
console.log(firstDeveloper.salary);



const secondDeveloper = new Programmer('Ivan', 40, 7000, [['ts',"js"], ['c++', 'c#'], 'java', 'python']);
secondDeveloper.name = 'Nick';
secondDeveloper.age = 41;
secondDeveloper.salary = 1500;
console.log(secondDeveloper.age);
console.log(secondDeveloper.salary);




