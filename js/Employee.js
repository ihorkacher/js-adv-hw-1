export class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;

    }
    get name() {
        return `Name - ${this._name}`;
    }
    set name(newName) {
        return this._name = newName;
    }

    get age() {
        return `Age - ${this._age}`;
    }
    set age(newAge) {
        return this._age = newAge;
    }

    get salary() {
        return `Salary - $${this._salary}`;
    }
    set salary(newSalary) {
        return this._salary = newSalary;
    }

}